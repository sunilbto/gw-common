/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author renuka.bhagwat
 *
 */
public class CategoryDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String catagoryType;
	private String description;

	/**
	 * @return the catagoryType
	 */
	public String getCatagoryType() {
		return catagoryType;
	}

	/**
	 * @param catagoryType
	 *            the catagoryType to set
	 */
	public void setCatagoryType(String catagoryType) {
		this.catagoryType = catagoryType;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
