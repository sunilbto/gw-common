/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author renuka.bhagwat
 *
 */
public class ApiKeyDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String merchantCode;
	private String apiKey;

	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode() {
		return merchantCode;
	}

	/**
	 * @param merchantCode
	 *            the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}

	/**
	 * @param apiKey
	 *            the apiKey to set
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ApiKeyDto [merchantCode=" + merchantCode + ", apiKey=" + apiKey + "]";
	}

}
