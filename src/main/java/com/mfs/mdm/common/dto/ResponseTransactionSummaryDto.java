package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.List;

public class ResponseTransactionSummaryDto  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String status;
	private List<TransactionSummaryDto> transactionList;
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the transactionList
	 */
	public List<TransactionSummaryDto> getTransactionList() {
		return transactionList;
	}
	/**
	 * @param transactionList the transactionList to set
	 */
	public void setTransactionList(List<TransactionSummaryDto> transactionList) {
		this.transactionList = transactionList;
	}
	
	
}
