/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author renuka.bhagwat
 *
 */
public class ResponseMerchantDto implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;

	private ResponseStatusDto status;
	private List<MerchantDtoForList> merchant;

	/**
	 * @return the status
	 */
	public ResponseStatusDto getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(ResponseStatusDto status) {
		this.status = status;
	}

	public List<MerchantDtoForList> getMerchant() {
		return merchant;
	}

	public void setMerchant(List<MerchantDtoForList> merchant) {
		this.merchant = merchant;
	}	
}
