package com.mfs.mdm.common.dto;

import java.io.Serializable;

public class MFSCountryDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String countryName;
	private String countryCode;
	private MFSCurrencyDto currency;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public MFSCurrencyDto getCurrency() {
		return currency;
	}

	public void setCurrency(MFSCurrencyDto currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "MFSCountryDto [countryName=" + countryName + ", countryCode=" + countryCode + ", currency=" + currency
				+ "]";
	}

}
