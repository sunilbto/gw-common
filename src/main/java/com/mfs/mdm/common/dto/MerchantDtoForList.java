package com.mfs.mdm.common.dto;

import java.io.Serializable;

public class MerchantDtoForList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String merchantCode;
	private String mechantName;
	private MFSCountryDto merchantCountry;
	private String merchantCategory;
	private String merchantDescription;
	private String merchantAccount;
	private String parentPartnerCode;

	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode() {
		return merchantCode;
	}

	/**
	 * @param merchantCode
	 *            the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	/**
	 * @return the mechantName
	 */
	public String getMechantName() {
		return mechantName;
	}

	/**
	 * @param mechantName
	 *            the mechantName to set
	 */
	public void setMechantName(String mechantName) {
		this.mechantName = mechantName;
	}

	/**
	 * @return the merchantCountry
	 */
	public MFSCountryDto getMerchantCountry() {
		return merchantCountry;
	}

	/**
	 * @param merchantCountry
	 *            the merchantCountry to set
	 */
	public void setMerchantCountry(MFSCountryDto merchantCountry) {
		this.merchantCountry = merchantCountry;
	}

	/**
	 * @return the merchantCategory
	 */
	public String getMerchantCategory() {
		return merchantCategory;
	}

	/**
	 * @param merchantCategory
	 *            the merchantCategory to set
	 */
	public void setMerchantCategory(String merchantCategory) {
		this.merchantCategory = merchantCategory;
	}

	/**
	 * @return the merchantDescription
	 */
	public String getMerchantDescription() {
		return merchantDescription;
	}

	/**
	 * @param merchantDescription
	 *            the merchantDescription to set
	 */
	public void setMerchantDescription(String merchantDescription) {
		this.merchantDescription = merchantDescription;
	}
	
	

	/**
	 * @return the merchantAccount
	 */
	public String getMerchantAccount() {
		return merchantAccount;
	}

	/**
	 * @param merchantAccount the merchantAccount to set
	 */
	public void setMerchantAccount(String merchantAccount) {
		this.merchantAccount = merchantAccount;
	}

	/**
	 * @return the parentPartnerCode
	 */
	public String getParentPartnerCode() {
		return parentPartnerCode;
	}

	/**
	 * @param parentPartnerCode the parentPartnerCode to set
	 */
	public void setParentPartnerCode(String parentPartnerCode) {
		this.parentPartnerCode = parentPartnerCode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MerchantDtoForList [merchantCode=" + merchantCode + ", mechantName=" + mechantName
				+ ", merchantCountry=" + merchantCountry + ", merchantCategory=" + merchantCategory
				+ ", merchantDescription=" + merchantDescription + ", merchantAccount=" + merchantAccount
				+ ", parentPartnerCode=" + parentPartnerCode + "]";
	}



}
