/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author nitin.chouhan
 *
 */
public class ResponseApiKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ResponseStatusDto status;
	
	private ApiKeyDto apiKeyResponse;

	/**
	 * @return the status
	 */
	public ResponseStatusDto getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(ResponseStatusDto status) {
		this.status = status;
	}

	/**
	 * @return the apiKeyResponse
	 */
	public ApiKeyDto getApiKeyResponse() {
		return apiKeyResponse;
	}

	/**
	 * @param apiKeyResponse the apiKeyResponse to set
	 */
	public void setApiKeyResponse(ApiKeyDto apiKeyResponse) {
		this.apiKeyResponse = apiKeyResponse;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResponseApiKey [status=" + status + ", apiKeyResponse=" + apiKeyResponse + "]";
	}
	
	
}
