/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author renuka.bhagwat
 *
 */
public class TransactionSummaryDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date billCreatedDate;
	private Date billPayDate;
	private String billId;
	private String mfsBillId;
	private Double billAmount;
	private String billCurrency;
	private String merchantMsisdn;
	private String merchantCode;
	private String spCode;
	private String rpCode;
	private String status;
	private String merchantAccount;
	private String senderMsisdn;
	private String statusCode;
	private String statusMessage;

	/**
	 * @return the billCreatedDate
	 */
	public Date getBillCreatedDate() {
		return billCreatedDate;
	}

	/**
	 * @param billCreatedDate
	 *            the billCreatedDate to set
	 */
	public void setBillCreatedDate(Date billCreatedDate) {
		this.billCreatedDate = billCreatedDate;
	}

	/**
	 * @return the billPayDate
	 */
	public Date getBillPayDate() {
		return billPayDate;
	}

	/**
	 * @param billPayDate
	 *            the billPayDate to set
	 */
	public void setBillPayDate(Date billPayDate) {
		this.billPayDate = billPayDate;
	}

	/**
	 * @return the billId
	 */
	public String getBillId() {
		return billId;
	}

	/**
	 * @param billId
	 *            the billId to set
	 */
	public void setBillId(String billId) {
		this.billId = billId;
	}

	/**
	 * @return the mfsBillId
	 */
	public String getMfsBillId() {
		return mfsBillId;
	}

	/**
	 * @param mfsBillId
	 *            the mfsBillId to set
	 */
	public void setMfsBillId(String mfsBillId) {
		this.mfsBillId = mfsBillId;
	}

	/**
	 * @return the billAmount
	 */
	public Double getBillAmount() {
		return billAmount;
	}

	/**
	 * @param billAmount
	 *            the billAmount to set
	 */
	public void setBillAmount(Double billAmount) {
		this.billAmount = billAmount;
	}

	/**
	 * @return the merchantMsisdn
	 */
	public String getMerchantMsisdn() {
		return merchantMsisdn;
	}

	/**
	 * @param merchantMsisdn
	 *            the merchantMsisdn to set
	 */
	public void setMerchantMsisdn(String merchantMsisdn) {
		this.merchantMsisdn = merchantMsisdn;
	}

	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode() {
		return merchantCode;
	}

	/**
	 * @param merchantCode
	 *            the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	/**
	 * @return the spCode
	 */
	public String getSpCode() {
		return spCode;
	}

	/**
	 * @param spCode
	 *            the spCode to set
	 */
	public void setSpCode(String spCode) {
		this.spCode = spCode;
	}

	/**
	 * @return the rpCode
	 */
	public String getRpCode() {
		return rpCode;
	}

	/**
	 * @param rpCode
	 *            the rpCode to set
	 */
	public void setRpCode(String rpCode) {
		this.rpCode = rpCode;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	/**
	 * @return the billCurrency
	 */
	public String getBillCurrency() {
		return billCurrency;
	}

	/**
	 * @param billCurrency
	 *            the billCurrency to set
	 */
	public void setBillCurrency(String billCurrency) {
		this.billCurrency = billCurrency;
	}

	/**
	 * @return the merchantAccount
	 */
	public String getMerchantAccount() {
		return merchantAccount;
	}

	/**
	 * @param merchantAccount
	 *            the merchantAccount to set
	 */
	public void setMerchantAccount(String merchantAccount) {
		this.merchantAccount = merchantAccount;
	}

	/**
	 * @return the senderMsisdn
	 */
	public String getSenderMsisdn() {
		return senderMsisdn;
	}

	/**
	 * @param senderMsisdn
	 *            the senderMsisdn to set
	 */
	public void setSenderMsisdn(String senderMsisdn) {
		this.senderMsisdn = senderMsisdn;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode
	 *            the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * @param statusMessage
	 *            the statusMessage to set
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionSummaryDto [billCreatedDate=" + billCreatedDate + ", billPayDate=" + billPayDate
				+ ", billId=" + billId + ", mfsBillId=" + mfsBillId + ", billAmount=" + billAmount + ", billCurrency="
				+ billCurrency + ", merchantMsisdn=" + merchantMsisdn + ", merchantCode=" + merchantCode + ", spCode="
				+ spCode + ", rpCode=" + rpCode + ", status=" + status + ", merchantAccount=" + merchantAccount
				+ ", senderMsisdn=" + senderMsisdn + ", statusCode=" + statusCode + ", statusMessage=" + statusMessage
				+ "]";
	}

}
