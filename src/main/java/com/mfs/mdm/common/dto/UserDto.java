/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author apurva.patil
 * 
 */
public class UserDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private ProfileDto profileDto;
	private OrganisationProfileDto orgProfileDto;
	private String password;
	private boolean passwordStatus;
	private RolesDto roles;
	private boolean accountLock;
	private boolean activeUser;
	private String groupName;
	private Integer customerId;
	private String resetPassword;
	private Date resetPasswordCreateDate;
	private String removeUserFlag;
	private String emailUrl;

	/**
	 * @return the profileDto
	 */
	public ProfileDto getProfileDto() {
		return profileDto;
	}

	/**
	 * @param profileDto
	 *            the profileDto to set
	 */
	public void setProfileDto(ProfileDto profileDto) {
		this.profileDto = profileDto;
	}

	/**
	 * @return the orgProfileDto
	 */
	public OrganisationProfileDto getOrgProfileDto() {
		return orgProfileDto;
	}

	/**
	 * @param orgProfileDto
	 *            the orgProfileDto to set
	 */
	public void setOrgProfileDto(OrganisationProfileDto orgProfileDto) {
		this.orgProfileDto = orgProfileDto;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the passwordStatus
	 */
	public boolean isPasswordStatus() {
		return passwordStatus;
	}

	/**
	 * @param passwordStatus the passwordStatus to set
	 */
	public void setPasswordStatus(boolean passwordStatus) {
		this.passwordStatus = passwordStatus;
	}

	/**
	 * @return the roles
	 */
	public RolesDto getRoles() {
		return roles;
	}

	/**
	 * @param roles
	 *            the roles to set
	 */
	public void setRoles(RolesDto roles) {
		this.roles = roles;
	}

	/**
	 * @return the accountLock
	 */
	public boolean isAccountLock() {
		return accountLock;
	}

	/**
	 * @param accountLock
	 *            the accountLock to set
	 */
	public void setAccountLock(boolean accountLock) {
		this.accountLock = accountLock;
	}

	/**
	 * @return the activeUser
	 */
	public boolean isActiveUser() {
		return activeUser;
	}

	/**
	 * @param activeUser
	 *            the activeUser to set
	 */
	public void setActiveUser(boolean activeUser) {
		this.activeUser = activeUser;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName
	 *            the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the customerId
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the resetPassword
	 */
	public String getResetPassword() {
		return resetPassword;
	}

	/**
	 * @param resetPassword
	 *            the resetPassword to set
	 */
	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}

	/**
	 * @return the resetPasswordCreateDate
	 */
	public Date getResetPasswordCreateDate() {
		return resetPasswordCreateDate;
	}

	/**
	 * @param resetPasswordCreateDate
	 *            the resetPasswordCreateDate to set
	 */
	public void setResetPasswordCreateDate(Date resetPasswordCreateDate) {
		this.resetPasswordCreateDate = resetPasswordCreateDate;
	}

	/**
	 * @return the removeUserFlag
	 */
	public String getRemoveUserFlag() {
		return removeUserFlag;
	}

	/**
	 * @param removeUserFlag
	 *            the removeUserFlag to set
	 */
	public void setRemoveUserFlag(String removeUserFlag) {
		this.removeUserFlag = removeUserFlag;
	}

	/**
	 * @return the emailUrl
	 */
	public String getEmailUrl() {
		return emailUrl;
	}

	/**
	 * @param emailUrl the emailUrl to set
	 */
	public void setEmailUrl(String emailUrl) {
		this.emailUrl = emailUrl;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserDto [profileDto=" + profileDto + ", orgProfileDto=" + orgProfileDto + ", password=" + password
				+ ", passwordStatus=" + passwordStatus + ", roles=" + roles + ", accountLock=" + accountLock
				+ ", activeUser=" + activeUser + ", groupName=" + groupName + ", customerId=" + customerId
				+ ", resetPassword=" + resetPassword + ", resetPasswordCreateDate=" + resetPasswordCreateDate
				+ ", removeUserFlag=" + removeUserFlag + ", emailUrl=" + emailUrl + "]";
	}

	

	

}
