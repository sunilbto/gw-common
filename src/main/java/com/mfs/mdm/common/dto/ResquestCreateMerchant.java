package com.mfs.mdm.common.dto;

import java.io.Serializable;

public class ResquestCreateMerchant implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mechantName;
	private String merchantCountry;
	private String merchantCategory;
	private String merchantDescription;

	/**
	 * @return the mechantName
	 */
	public String getMechantName() {
		return mechantName;
	}

	/**
	 * @param mechantName
	 *            the mechantName to set
	 */
	public void setMechantName(String mechantName) {
		this.mechantName = mechantName;
	}

	/**
	 * @return the merchantCountry
	 */
	public String getMerchantCountry() {
		return merchantCountry;
	}

	/**
	 * @param merchantCountry
	 *            the merchantCountry to set
	 */
	public void setMerchantCountry(String merchantCountry) {
		this.merchantCountry = merchantCountry;
	}

	/**
	 * @return the merchantCategory
	 */
	public String getMerchantCategory() {
		return merchantCategory;
	}

	/**
	 * @param merchantCategory
	 *            the merchantCategory to set
	 */
	public void setMerchantCategory(String merchantCategory) {
		this.merchantCategory = merchantCategory;
	}

	/**
	 * @return the merchantDescription
	 */
	public String getMerchantDescription() {
		return merchantDescription;
	}

	/**
	 * @param merchantDescription
	 *            the merchantDescription to set
	 */
	public void setMerchantDescription(String merchantDescription) {
		this.merchantDescription = merchantDescription;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResquestCreateMerchant [mechantName=" + mechantName + ", merchantCountry=" + merchantCountry
				+ ", merchantCategory=" + merchantCategory + ", merchantDescription=" + merchantDescription + "]";
	}

}
