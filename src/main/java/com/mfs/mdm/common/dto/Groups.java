package com.mfs.mdm.common.dto;

import java.util.Date;
import java.util.List;

public class Groups {
	private int groupId;
	private String groupName;
	private Date ceationDate;
	private String groupDescription;
	
	
	

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Date getCeationDate() {
		return ceationDate;
	}

	public void setCeationDate(Date ceationDate) {
		this.ceationDate = ceationDate;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	@Override
	public String toString() {
		return "Groups [groupId=" + groupId + ", groupName=" + groupName + ", ceationDate=" + ceationDate
				+ ", groupDescription=" + groupDescription + "]";
	}

}
