/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author renuka.bhagwat
 *
 */
public class ResponseCreateMerchant implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ResponseStatusDto status;

	private String mfsMerchantCode;

	private String merchantName;

	/**
	 * @return the status
	 */
	public ResponseStatusDto getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(ResponseStatusDto status) {
		this.status = status;
	}

	/**
	 * @return the mfsMerchantCode
	 */
	public String getMfsMerchantCode() {
		return mfsMerchantCode;
	}

	/**
	 * @param mfsMerchantCode
	 *            the mfsMerchantCode to set
	 */
	public void setMfsMerchantCode(String mfsMerchantCode) {
		this.mfsMerchantCode = mfsMerchantCode;
	}

	/**
	 * @return the merchantName
	 */
	public String getMerchantName() {
		return merchantName;
	}

	/**
	 * @param merchantName
	 *            the merchantName to set
	 */
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResponseCreateMerchant [status=" + status + ", mfsMerchantCode=" + mfsMerchantCode + ", merchantName="
				+ merchantName + "]";
	}

}
