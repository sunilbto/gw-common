/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author nitin.chouhan
 *
 */
public class ResponseUpdateMerchant implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String merchantCode;
	private String parentPartnerCode;
	private String merchantName;
	private String merchantAccount;
	private String merchantDescription;
	private ResponseStatusDto status;
	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode() {
		return merchantCode;
	}
	/**
	 * @param merchantCode the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	/**
	 * @return the parentPartnerCode
	 */
	public String getParentPartnerCode() {
		return parentPartnerCode;
	}
	/**
	 * @param parentPartnerCode the parentPartnerCode to set
	 */
	public void setParentPartnerCode(String parentPartnerCode) {
		this.parentPartnerCode = parentPartnerCode;
	}
	/**
	 * @return the merchantName
	 */
	public String getMerchantName() {
		return merchantName;
	}
	/**
	 * @param merchantName the merchantName to set
	 */
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	/**
	 * @return the merchantAccount
	 */
	public String getMerchantAccount() {
		return merchantAccount;
	}
	/**
	 * @param merchantAccount the merchantAccount to set
	 */
	public void setMerchantAccount(String merchantAccount) {
		this.merchantAccount = merchantAccount;
	}
	/**
	 * @return the status
	 */
	public ResponseStatusDto getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(ResponseStatusDto status) {
		this.status = status;
	}
	/**
	 * @return the merchantDescription
	 */
	public String getMerchantDescription() {
		return merchantDescription;
	}
	/**
	 * @param merchantDescription the merchantDescription to set
	 */
	public void setMerchantDescription(String merchantDescription) {
		this.merchantDescription = merchantDescription;
	}
	
	
}
