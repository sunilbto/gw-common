package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author apurva.patil
 *
 */
public class AddressDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String address;
	private String address1;
	private String address2;
	private String city;
	private String postalCode;
	private String stateRegion;
	private CountryDto country;

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1
	 *            the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2
	 *            the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the stateRegion
	 */
	public String getStateRegion() {
		return stateRegion;
	}

	/**
	 * @param stateRegion
	 *            the stateRegion to set
	 */
	public void setStateRegion(String stateRegion) {
		this.stateRegion = stateRegion;
	}

	/**
	 * @return the country
	 */
	public CountryDto getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(CountryDto country) {
		this.country = country;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AddressDto [address=" + address + ", address1=" + address1 + ", address2=" + address2 + ", postalCode="
				+ postalCode + ", stateRegion=" + stateRegion + ", country=" + country + "]";
	}

		

}
