/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author renuka.bhagwat
 *
 */
public class ParentPartnerDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String partnerName;
	private String partnerCode;

	/**
	 * @return the partnerName
	 */
	public String getPartnerName() {
		return partnerName;
	}

	/**
	 * @param partnerName
	 *            the partnerName to set
	 */
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	/**
	 * @return the partnerCode
	 */
	public String getPartnerCode() {
		return partnerCode;
	}

	/**
	 * @param partnerCode
	 *            the partnerCode to set
	 */
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParentPartnerDto [partnerName=" + partnerName + ", partnerCode=" + partnerCode + "]";
	}

	
}
