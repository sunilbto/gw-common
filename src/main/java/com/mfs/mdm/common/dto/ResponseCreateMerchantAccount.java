/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author renuka.bhagwat
 *
 */
public class ResponseCreateMerchantAccount implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<GetMerchantResponseDto> merchant;
	private ResponseStatusDto status;
	/**
	 * @return the merchant
	 */
	public List<GetMerchantResponseDto> getMerchant() {
		return merchant;
	}
	/**
	 * @param merchant the merchant to set
	 */
	public void setMerchant(List<GetMerchantResponseDto> merchant) {
		this.merchant = merchant;
	}
	/**
	 * @return the status
	 */
	public ResponseStatusDto getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(ResponseStatusDto status) {
		this.status = status;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResponseCreateMerchantAccount [merchant=" + merchant + ", status=" + status + "]";
	}
	
}
