/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author Nitin.Chouhan
 *
 */
public class TransactionSummaryByDateDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<TransactionDateSummaryDto> listTransactionDateSummaryDto;
	
	private Integer totalTransactionCount;
	
	private Double totalTransactionAmount;
	
	private String currency;

	/**
	 * @return the listTransactionDateSummaryDto
	 */
	public List<TransactionDateSummaryDto> getListTransactionDateSummaryDto() {
		return listTransactionDateSummaryDto;
	}

	/**
	 * @param listTransactionDateSummaryDto the listTransactionDateSummaryDto to set
	 */
	public void setListTransactionDateSummaryDto(List<TransactionDateSummaryDto> listTransactionDateSummaryDto) {
		this.listTransactionDateSummaryDto = listTransactionDateSummaryDto;
	}

	/**
	 * @return the totalTransactionCount
	 */
	public Integer getTotalTransactionCount() {
		return totalTransactionCount;
	}

	/**
	 * @param totalTransactionCount the totalTransactionCount to set
	 */
	public void setTotalTransactionCount(Integer totalTransactionCount) {
		this.totalTransactionCount = totalTransactionCount;
	}

	/**
	 * @return the totalTransactionAmount
	 */
	public Double getTotalTransactionAmount() {
		return totalTransactionAmount;
	}

	/**
	 * @param totalTransactionAmount the totalTransactionAmount to set
	 */
	public void setTotalTransactionAmount(Double totalTransactionAmount) {
		this.totalTransactionAmount = totalTransactionAmount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}



}
