package com.mfs.mdm.common.dto;

import java.io.Serializable;

public class AuditLogInputDto implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String fromDate;
	
	private  String toDate; 
	
	private String orgnisationName;

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the orgnisationName
	 */
	public String getOrgnisationName() {
		return orgnisationName;
	}

	/**
	 * @param orgnisationName the orgnisationName to set
	 */
	public void setOrgnisationName(String orgnisationName) {
		this.orgnisationName = orgnisationName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AuditLogInputDto [fromDate=" + fromDate + ", toDate=" + toDate + ", orgnisationName=" + orgnisationName
				+ "]";
	}

	
	
}
