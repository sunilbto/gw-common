package com.mfs.mdm.common.dto;

import java.io.Serializable;

public class MFSCurrencyDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String currencyCode;
	private String description;

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "MFSCurrencyDto [currencyCode=" + currencyCode + ", description=" + description + "]";
	}

}
