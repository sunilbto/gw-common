/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author nitin.chouhan
 *
 */
public class ApikeyRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String merchantCode;

	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode() {
		return merchantCode;
	}

	/**
	 * @param merchantCode
	 *            the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RequestApikey [merchantCode=" + merchantCode + "]";
	}

}
