/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author renuka.bhagwat
 *
 */
public class RequestUpdateMerchantAccount implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String merchantAccount;
	private String mfsMerchantCode;
	private String mechantName;
	private String merchantDescription;
	private String parentPartnerCode;
	private String merchantCountry;

	/**
	 * @return the merchantAccount
	 */
	public String getMerchantAccount() {
		return merchantAccount;
	}

	/**
	 * @param merchantAccount
	 *            the merchantAccount to set
	 */
	public void setMerchantAccount(String merchantAccount) {
		this.merchantAccount = merchantAccount;
	}

	/**
	 * @return the mfsMerchantCode
	 */
	public String getMfsMerchantCode() {
		return mfsMerchantCode;
	}

	/**
	 * @param mfsMerchantCode
	 *            the mfsMerchantCode to set
	 */
	public void setMfsMerchantCode(String mfsMerchantCode) {
		this.mfsMerchantCode = mfsMerchantCode;
	}

	/**
	 * @return the mechantName
	 */
	public String getMechantName() {
		return mechantName;
	}

	/**
	 * @param mechantName
	 *            the mechantName to set
	 */
	public void setMechantName(String mechantName) {
		this.mechantName = mechantName;
	}

	/**
	 * @return the merchantDescription
	 */
	public String getMerchantDescription() {
		return merchantDescription;
	}

	/**
	 * @param merchantDescription
	 *            the merchantDescription to set
	 */
	public void setMerchantDescription(String merchantDescription) {
		this.merchantDescription = merchantDescription;
	}

	
	/**
	 * @return the parentPartnerCode
	 */
	public String getParentPartnerCode() {
		return parentPartnerCode;
	}

	/**
	 * @param parentPartnerCode the parentPartnerCode to set
	 */
	public void setParentPartnerCode(String parentPartnerCode) {
		this.parentPartnerCode = parentPartnerCode;
	}

	/**
	 * @return the merchantCountry
	 */
	public String getMerchantCountry() {
		return merchantCountry;
	}

	/**
	 * @param merchantCountry the merchantCountry to set
	 */
	public void setMerchantCountry(String merchantCountry) {
		this.merchantCountry = merchantCountry;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RequestUpdateMerchantAccount [merchantAccount=" + merchantAccount + ", mfsMerchantCode="
				+ mfsMerchantCode + ", mechantName=" + mechantName + ", merchantDescription=" + merchantDescription
				+ ", parentPartnerCode=" + parentPartnerCode + ", merchantCountry=" + merchantCountry + "]";
	}



}
