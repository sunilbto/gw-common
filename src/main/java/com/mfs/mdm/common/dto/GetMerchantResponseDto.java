package com.mfs.mdm.common.dto;

import java.io.Serializable;

public class GetMerchantResponseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String merchantAccount;
	private String merchantCode;
	private String mechantName;
	private String parentPartnerCode;

	
	/**
	 * @return the merchantAccount
	 */
	public String getMerchantAccount() {
		return merchantAccount;
	}

	/**
	 * @param merchantAccount
	 *            the merchantAccount to set
	 */
	public void setMerchantAccount(String merchantAccount) {
		this.merchantAccount = merchantAccount;
	}

	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode() {
		return merchantCode;
	}

	/**
	 * @param merchantCode
	 *            the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	/**
	 * @return the mechantName
	 */
	public String getMechantName() {
		return mechantName;
	}

	/**
	 * @param mechantName
	 *            the mechantName to set
	 */
	public void setMechantName(String mechantName) {
		this.mechantName = mechantName;
	}

	/**
	 * @return the parentPartnerCode
	 */
	public String getParentPartnerCode() {
		return parentPartnerCode;
	}

	/**
	 * @param parentPartnerCode
	 *            the parentPartnerCode to set
	 */
	public void setParentPartnerCode(String parentPartnerCode) {
		this.parentPartnerCode = parentPartnerCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GetMerchantResponseDto [merchantAccount=" + merchantAccount + ", merchantCode=" + merchantCode
				+ ", mechantName=" + mechantName + ", parentPartnerCode=" + parentPartnerCode + "]";
	}

}
