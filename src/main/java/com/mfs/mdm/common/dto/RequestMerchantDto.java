/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author renuka.bhagwat
 *
 */
public class RequestMerchantDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mechantName;
	private String merchantCategory;
	private String merchantDescription;
	private String merchantAccount;
	
	private MFSCountryDto merchantCountry;
	private String parentPartnerCode;
	private String merchantCode;

	public String getMechantName() {
		return mechantName;
	}

	public void setMechantName(String mechantName) {
		this.mechantName = mechantName;
	}

	/**
	 * @return the merchantCategory
	 */
	public String getMerchantCategory() {
		return merchantCategory;
	}

	/**
	 * @param merchantCategory
	 *            the merchantCategory to set
	 */
	public void setMerchantCategory(String merchantCategory) {
		this.merchantCategory = merchantCategory;
	}

	/**
	 * @return the merchantDescription
	 */
	public String getMerchantDescription() {
		return merchantDescription;
	}

	/**
	 * @param merchantDescription
	 *            the merchantDescription to set
	 */
	public void setMerchantDescription(String merchantDescription) {
		this.merchantDescription = merchantDescription;
	}

	/**
	 * @return the merchantAccount
	 */
	public String getMerchantAccount() {
		return merchantAccount;
	}

	/**
	 * @param merchantAccount
	 *            the merchantAccount to set
	 */
	public void setMerchantAccount(String merchantAccount) {
		this.merchantAccount = merchantAccount;
	}

	public MFSCountryDto getMerchantCountry() {
		return merchantCountry;
	}

	public void setMerchantCountry(MFSCountryDto merchantCountry) {
		this.merchantCountry = merchantCountry;
	}

	/**
	 * @return the parentPartnerCode
	 */
	public String getParentPartnerCode() {
		return parentPartnerCode;
	}

	/**
	 * @param parentPartnerCode
	 *            the parentPartnerCode to set
	 */
	public void setParentPartnerCode(String parentPartnerCode) {
		this.parentPartnerCode = parentPartnerCode;
	}

	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode() {
		return merchantCode;
	}

	/**
	 * @param merchantCode
	 *            the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	@Override
	public String toString() {
		return "RequestMerchantDto [mechantName=" + mechantName + ", merchantCategory=" + merchantCategory
				+ ", merchantDescription=" + merchantDescription + ", merchantAccount=" + merchantAccount
				+ ", merchantCountry=" + merchantCountry + ", parentPartnerCode=" + parentPartnerCode
				+ ", merchantCode=" + merchantCode + "]";
	}

}
