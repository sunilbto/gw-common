/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.Date;

import com.mfs.mdm.common.type.ReportType;

/**
 * @author renuka.bhagwat
 *
 */
public class ReportsInputDto implements Serializable{
	
	
	private static final long serialVersionUID = 1L;

	private Date fromDate;
	
	private  Date toDate; 
	
	private String orgnisationName;
	
	private String transactionType;
	
	private String merchantCode;
	
	private String reportType;

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the orgnisationName
	 */
	public String getOrgnisationName() {
		return orgnisationName;
	}

	/**
	 * @param orgnisationName the orgnisationName to set
	 */
	public void setOrgnisationName(String orgnisationName) {
		this.orgnisationName = orgnisationName;
	}

	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}

	/**
	 * @param reportType the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode() {
		return merchantCode;
	}

	/**
	 * @param merchantCode the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	

}
