/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author renuka.bhagwat
 *
 */
public class MerchantResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ResponseStatusDto status;
	private List<RequestMerchantDto> merchant;

	public ResponseStatusDto getStatus() {
		return status;
	}

	public void setStatus(ResponseStatusDto status) {
		this.status = status;
	}

	public List<RequestMerchantDto> getMerchant() {
		return merchant;
	}

	public void setMerchant(List<RequestMerchantDto> merchant) {
		this.merchant = merchant;
	}

	@Override
	public String toString() {
		return "MerchantResponse [status=" + status + ", merchant=" + merchant + "]";
	}

}
