/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author renuka.bhagwat
 *
 */
public class RequestTransactionDetailDto  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String  mfsBillId;

	/**
	 * @return the mfsBillId
	 */
	public String getMfsBillId() {
		return mfsBillId;
	}

	/**
	 * @param mfsBillId the mfsBillId to set
	 */
	public void setMfsBillId(String mfsBillId) {
		this.mfsBillId = mfsBillId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RequestTransactionSummary [mfsBillId=" + mfsBillId + "]";
	}
	
}
