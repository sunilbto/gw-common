/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author apurva.patil
 * 
 */
public class ProfileDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String firstName;
	private String lastName;
	private String email;
	private boolean isEmailVerified;
	private String mobileNumber;
	private Date dateOfBirth;
	private List<AddressDto> addressDtoList;
	private TimeZoneDto timeZoneDto;
	private IdDto idDto;
	private String status;
	private String countryCode;
	private List<IdDto> idTypeList;

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the isEmailVerified
	 */
	public boolean isEmailVerified() {
		return isEmailVerified;
	}

	/**
	 * @param isEmailVerified the isEmailVerified to set
	 */
	public void setEmailVerified(boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public List<AddressDto> getAddressDtoList() {
		return addressDtoList;
	}

	public void setAddressDtoList(List<AddressDto> addressDtoList) {
		this.addressDtoList = addressDtoList;
	}

	/**
	 * @return the timeZoneDto
	 */
	public TimeZoneDto getTimeZoneDto() {
		return timeZoneDto;
	}

	/**
	 * @param timeZoneDto the timeZoneDto to set
	 */
	public void setTimeZoneDto(TimeZoneDto timeZoneDto) {
		this.timeZoneDto = timeZoneDto;
	}

	/**
	 * @return the idDto
	 */
	public IdDto getIdDto() {
		return idDto;
	}

	/**
	 * @param idDto the idDto to set
	 */
	public void setIdDto(IdDto idDto) {
		this.idDto = idDto;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public List<IdDto> getIdTypeList() {
		return idTypeList;
	}

	public void setIdTypeList(List<IdDto> idTypeList) {
		this.idTypeList = idTypeList;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		return "ProfileDto [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", isEmailVerified=" + isEmailVerified + ", mobileNumber=" + mobileNumber + ", dateOfBirth="
				+ dateOfBirth + ", addressDtoList=" + addressDtoList + ", timeZoneDto=" + timeZoneDto + ", idDto="
				+ idDto + ", status=" + status + ", countryCode=" + countryCode + ", idTypeList=" + idTypeList + "]";
	}

}
