package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author apurva.patil
 *
 */
public class CurrencyDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String currencyName;
	private String currencyCode;
	private CountryDto countryDto;

	/**
	 * @return the currencyName
	 */
	public String getCurrencyName() {
		return currencyName;
	}

	/**
	 * @param currencyName
	 *            the currencyName to set
	 */
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the countryDto
	 */
	public CountryDto getCountryDto() {
		return countryDto;
	}

	/**
	 * @param countryDto
	 *            the countryDto to set
	 */
	public void setCountryDto(CountryDto countryDto) {
		this.countryDto = countryDto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CurrencyDto [currencyName=" + currencyName + ", currencyCode=" + currencyCode + ", countryDto="
				+ countryDto + "]";
	}

}
