package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author apurva.patil
 *
 */
public class IdDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idNumber;
	private String idType;
	private String idExpiryDate;

	/**
	 * @return the idNumber
	 */
	public String getIdNumber() {
		return idNumber;
	}

	/**
	 * @param idNumber
	 *            the idNumber to set
	 */
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	/**
	 * @return the idType
	 */
	public String getIdType() {
		return idType;
	}

	/**
	 * @param idType
	 *            the idType to set
	 */
	public void setIdType(String idType) {
		this.idType = idType;
	}

	/**
	 * @return the idExpiryDate
	 */
	public String getIdExpiryDate() {
		return idExpiryDate;
	}

	/**
	 * @param idExpiryDate
	 *            the idExpiryDate to set
	 */
	public void setIdExpiryDate(String idExpiryDate) {
		this.idExpiryDate = idExpiryDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IdDto [idNumber=" + idNumber + ", idType=" + idType + ", idExpiryDate=" + idExpiryDate + "]";
	}

}
