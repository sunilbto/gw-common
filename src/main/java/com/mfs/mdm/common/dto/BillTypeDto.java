/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author nitin.chouhan
 *
 */
public class BillTypeDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String bill_type;
	private String description;

	/**
	 * @return the bill_type
	 */
	public String getBill_type() {
		return bill_type;
	}

	/**
	 * @param bill_type
	 *            the bill_type to set
	 */
	public void setBill_type(String bill_type) {
		this.bill_type = bill_type;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
