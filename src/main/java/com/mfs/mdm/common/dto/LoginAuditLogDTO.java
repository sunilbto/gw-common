package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.Date;

public class LoginAuditLogDTO implements Serializable {

	/**
	 * @author renuka.bhagwat
	 */
	private static final long serialVersionUID = 6276632883313618760L;
	private String email;
	private String ipAddress;
	private Date dateTime;
	private String eventResult;
	private String description;
	private String orgName;

	public String getEmail() {
		return email;
	}

	public void setEmail(String username) {
		this.email = username;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getEventResult() {
		return eventResult;
	}

	public void setEventResult(String eventResult) {
		this.eventResult = eventResult;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	/**
	 * @return the orgName
	 */
	public String getOrgName() {
		return orgName;
	}

	/**
	 * @param orgName the orgName to set
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	@Override
	public String toString() {
		return "LoginAuditLogDTO [email=" + email + ", ipAddress="
				+ ipAddress + ", dateTime=" + dateTime + ", eventResult="
				+ eventResult + ", description=" + description + "]";
	}

}
