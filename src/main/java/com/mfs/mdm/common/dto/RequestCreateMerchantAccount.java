package com.mfs.mdm.common.dto;

/**
 * Class to request for create merchant account
 * @author nitin.chouhan
 *
 */

public class RequestCreateMerchantAccount {

	private String mechantName;
	private String merchantCategory;
	private String merchantDescription;
	private String merchantAccount;
	private String merchantCountry;
	private String parentPartnerCode;
	private String merchantCode;
	/**
	 * @return the merchantName
	 */
	public String getMechantName() {
		return mechantName;
	}
	/**
	 * @param merchantName the merchantName to set
	 */
	public void setMechantName(String mechantName) {
		this.mechantName = mechantName;
	}
	/**
	 * @return the merchantCategory
	 */
	public String getMerchantCategory() {
		return merchantCategory;
	}
	/**
	 * @param merchantCategory the merchantCategory to set
	 */
	public void setMerchantCategory(String merchantCategory) {
		this.merchantCategory = merchantCategory;
	}
	/**
	 * @return the merchantDescription
	 */
	public String getMerchantDescription() {
		return merchantDescription;
	}
	/**
	 * @param merchantDescription the merchantDescription to set
	 */
	public void setMerchantDescription(String merchantDescription) {
		this.merchantDescription = merchantDescription;
	}
	/**
	 * @return the merchantAccount
	 */
	public String getMerchantAccount() {
		return merchantAccount;
	}
	/**
	 * @param merchantAccount the merchantAccount to set
	 */
	public void setMerchantAccount(String merchantAccount) {
		this.merchantAccount = merchantAccount;
	}
	/**
	 * @return the merchantCountry
	 */
	public String getMerchantCountry() {
		return merchantCountry;
	}
	/**
	 * @param merchantCountry the merchantCountry to set
	 */
	public void setMerchantCountry(String merchantCountry) {
		this.merchantCountry = merchantCountry;
	}
	/**
	 * @return the parentPartnerCode
	 */
	public String getParentPartnerCode() {
		return parentPartnerCode;
	}
	/**
	 * @param parentPartnerCode the parentPartnerCode to set
	 */
	public void setParentPartnerCode(String parentPartnerCode) {
		this.parentPartnerCode = parentPartnerCode;
	}
	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode() {
		return merchantCode;
	}
	/**
	 * @param merchantCode the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	
	
}
