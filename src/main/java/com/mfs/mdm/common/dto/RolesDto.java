package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author apurva.patil
 *
 */
public class RolesDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String roleName;
	private String roleDescription;
	private Date roleCreateDate;
	private String createdBy;
	private int roleId;

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * @param roleName
	 *            the roleName to set
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return the roleDescription
	 */
	public String getRoleDescription() {
		return roleDescription;
	}

	/**
	 * @param roleDescription
	 *            the roleDescription to set
	 */
	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	/**
	 * @return the roleCreateDate
	 */
	public Date getRoleCreateDate() {
		return roleCreateDate;
	}

	/**
	 * @param roleCreateDate
	 *            the roleCreateDate to set
	 */
	public void setRoleCreateDate(Date roleCreateDate) {
		this.roleCreateDate = roleCreateDate;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return "RolesDto [roleName=" + roleName + ", roleDescription=" + roleDescription + ", roleCreateDate="
				+ roleCreateDate + ", createdBy=" + createdBy + ", roleId=" + roleId + "]";
	}

}
