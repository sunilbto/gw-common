package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author apurva.patil
 *
 */
public class OrganisationProfileDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String organisationID; // change string to int
	private String organisationName;
	private String businessRegistrationNumber;
	private String description;
	private String typeUser;
	private String partnerCode;
	private List<AddressDto> addressDtoList; //
	private CurrencyDto currencyDto; //
	private String mobileNumber;
	private String email; // no set
	private String status;
	private String orgApiKey;
	private String parentPartner;
	private String billType;
	private String overrideBillType;
	private Date registrationDate;
	private Date modifiedOn;
	private String modifiedBy;
	private Groups groups;
	private String organisationType; // created new
	private String timezoneName; // create
	private String category;
	private String financeEmail;
	private String supportEmail;
	private String operationsEmail;
	private List<CountryDto> countryList;
	private String complianceOfficer;
	private String complianceEmail;
	private String compliancePhone;
	private String regulatorCountry;
	private String partnerType;
	private String zoneName;

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getRegulatorCountry() {
		return regulatorCountry;
	}

	public void setRegulatorCountry(String regulatorCountry) {
		this.regulatorCountry = regulatorCountry;
	}

	public String getComplianceOfficer() {
		return complianceOfficer;
	}

	public void setComplianceOfficer(String complianceOfficer) {
		this.complianceOfficer = complianceOfficer;
	}

	public String getComplianceEmail() {
		return complianceEmail;
	}

	public void setComplianceEmail(String complianceEmail) {
		this.complianceEmail = complianceEmail;
	}

	public String getCompliancePhone() {
		return compliancePhone;
	}

	public void setCompliancePhone(String compliancePhone) {
		this.compliancePhone = compliancePhone;
	}

	public List<CountryDto> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<CountryDto> countryList) {
		this.countryList = countryList;
	}

	public String getTypeUser() {
		return typeUser;
	}

	public void setTypeUser(String typeUser) {
		this.typeUser = typeUser;
	}

	public String getFinanceEmail() {
		return financeEmail;
	}

	public void setFinanceEmail(String financeEmail) {
		this.financeEmail = financeEmail;
	}

	public String getSupportEmail() {
		return supportEmail;
	}

	public void setSupportEmail(String supportEmail) {
		this.supportEmail = supportEmail;
	}

	public String getOperationsEmail() {
		return operationsEmail;
	}

	public void setOperationsEmail(String operationsEmail) {
		this.operationsEmail = operationsEmail;
	}

	/**
	 * @return the organisationName
	 */
	public String getOrganisationName() {
		return organisationName;
	}

	public String getOrganisationID() {
		return organisationID;
	}

	public void setOrganisationID(String organisationID) {
		this.organisationID = organisationID;
	}

	public String getOrganisationType() {
		return organisationType;
	}

	public void setOrganisationType(String organisationType) {
		this.organisationType = organisationType;
	}

	public String getTimezoneName() {
		return timezoneName;
	}

	public void setTimezoneName(String timezoneName) {
		this.timezoneName = timezoneName;
	}

	/**
	 * @param organisationName
	 *            the organisationName to set
	 */
	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	/**
	 * @return the businessRegistrationNumber
	 */
	public String getBusinessRegistrationNumber() {
		return businessRegistrationNumber;
	}

	/**
	 * @param businessRegistrationNumber
	 *            the businessRegistrationNumber to set
	 */
	public void setBusinessRegistrationNumber(String businessRegistrationNumber) {
		this.businessRegistrationNumber = businessRegistrationNumber;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the addressDtoList
	 */
	public List<AddressDto> getAddressDtoList() {
		return addressDtoList;
	}

	/**
	 * @param addressDtoList
	 *            the addressDtoList to set
	 */
	public void setAddressDtoList(List<AddressDto> addressDtoList) {
		this.addressDtoList = addressDtoList;
	}

	/**
	 * @return the currencyDto
	 */
	public CurrencyDto getCurrencyDto() {
		return currencyDto;
	}

	/**
	 * @param currencyDto
	 *            the currencyDto to set
	 */
	public void setCurrencyDto(CurrencyDto currencyDto) {
		this.currencyDto = currencyDto;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *            the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the orgApiKey
	 */
	public String getOrgApiKey() {
		return orgApiKey;
	}

	/**
	 * @param orgApiKey
	 *            the orgApiKey to set
	 */
	public void setOrgApiKey(String orgApiKey) {
		this.orgApiKey = orgApiKey;
	}

	/**
	 * @return the parentPartner
	 */
	public String getParentPartner() {
		return parentPartner;
	}

	/**
	 * @param parentPartner
	 *            the parentPartner to set
	 */
	public void setParentPartner(String parentPartner) {
		this.parentPartner = parentPartner;
	}

	/**
	 * @return the billType
	 */
	public String getBillType() {
		return billType;
	}

	/**
	 * @param billType
	 *            the billType to set
	 */
	public void setBillType(String billType) {
		this.billType = billType;
	}

	/**
	 * @return the overrideBillType
	 */
	public String getOverrideBillType() {
		return overrideBillType;
	}

	/**
	 * @param overrideBillType
	 *            the overrideBillType to set
	 */
	public void setOverrideBillType(String overrideBillType) {
		this.overrideBillType = overrideBillType;
	}

	/**
	 * @return the registrationDate
	 */
	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Groups getGroups() {
		return groups;
	}

	public void setGroups(Groups groups) {
		this.groups = groups;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	@Override
	public String toString() {
		return "OrganisationProfileDto [organisationID=" + organisationID + ", organisationName=" + organisationName
				+ ", businessRegistrationNumber=" + businessRegistrationNumber + ", description=" + description
				+ ", typeUser=" + typeUser + ", partnerCode=" + partnerCode + ", addressDtoList=" + addressDtoList
				+ ", currencyDto=" + currencyDto + ", mobileNumber=" + mobileNumber + ", email=" + email + ", status="
				+ status + ", orgApiKey=" + orgApiKey + ", parentPartner=" + parentPartner + ", billType=" + billType
				+ ", overrideBillType=" + overrideBillType + ", registrationDate=" + registrationDate + ", modifiedOn="
				+ modifiedOn + ", modifiedBy=" + modifiedBy + ", groups=" + groups + ", organisationType="
				+ organisationType + ", timezoneName=" + timezoneName + ", category=" + category + ", financeEmail="
				+ financeEmail + ", supportEmail=" + supportEmail + ", operationsEmail=" + operationsEmail
				+ ", countryList=" + countryList + ", complianceOfficer=" + complianceOfficer + ", complianceEmail="
				+ complianceEmail + ", compliancePhone=" + compliancePhone + ", regulatorCountry=" + regulatorCountry
				+ ", partnerType=" + partnerType + ", zoneName=" + zoneName + "]";
	}

}
