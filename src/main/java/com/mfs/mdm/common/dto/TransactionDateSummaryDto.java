/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Nitin.Chouhan
 *
 */
public class TransactionDateSummaryDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date billPayDate;
	private Integer transactionCount;
	private Double transactionAmount;
	/**
	 * @return the billPayDate
	 */
	public Date getBillPayDate() {
		return billPayDate;
	}
	/**
	 * @param billPayDate the billPayDate to set
	 */
	public void setBillPayDate(Date billPayDate) {
		this.billPayDate = billPayDate;
	}
	/**
	 * @return the transactionCount
	 */
	public Integer getTransactionCount() {
		return transactionCount;
	}
	/**
	 * @param transactionCount the transactionCount to set
	 */
	public void setTransactionCount(Integer transactionCount) {
		this.transactionCount = transactionCount;
	}
	/**
	 * @return the transactionAmount
	 */
	public Double getTransactionAmount() {
		return transactionAmount;
	}
	/**
	 * @param transactionAmount the transactionAmount to set
	 */
	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionSummaryByDateDto [billPayDate=" + billPayDate + ", transactionCount=" + transactionCount
				+ ", transactionAmount=" + transactionAmount + "]";
	}



}
