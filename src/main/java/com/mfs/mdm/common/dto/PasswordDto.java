package com.mfs.mdm.common.dto;

import java.io.Serializable;

public class PasswordDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String email;
	private String currentPassword;
	private String newPassword;
	private String oldPassword;

	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	@Override
	public String toString() {
		return "PasswordDto [email=" + email + ", currentPassword=" + currentPassword + ", newPassword=" + newPassword
				+ ", oldPassword=" + oldPassword + "]";
	}
	

}
