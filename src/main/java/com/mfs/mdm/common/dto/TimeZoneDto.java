package com.mfs.mdm.common.dto;

import java.io.Serializable;

/**
 * @author apurva.patil
 *
 */
public class TimeZoneDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String timeZoneName;
	private String timeZone;

	/**
	 * @return the timeZoneName
	 */
	public String getTimeZoneName() {
		return timeZoneName;
	}

	/**
	 * @param timeZoneName
	 *            the timeZoneName to set
	 */
	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}

	/**
	 * @return the timeZone
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone
	 *            the timeZone to set
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TimeZoneDto [timeZoneName=" + timeZoneName + ", timeZone=" + timeZone + "]";
	}

}
