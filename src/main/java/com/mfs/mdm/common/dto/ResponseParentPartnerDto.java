/**
 * 
 */
package com.mfs.mdm.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author renuka.bhagwat
 *
 */
public class ResponseParentPartnerDto  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ResponseStatusDto status;
	
	private List<ParentPartnerDto> partnerList;

	/**
	 * @return the partnerList
	 */
	public List<ParentPartnerDto> getPartnerList() {
		return partnerList;
	}

	/**
	 * @param partnerList the partnerList to set
	 */
	public void setPartnerList(List<ParentPartnerDto> partnerList) {
		this.partnerList = partnerList;
	}

	/**
	 * @return the status
	 */
	public ResponseStatusDto getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(ResponseStatusDto status) {
		this.status = status;
	}
	
	
	
}
