/**
 * 
 */
package com.mfs.mdm.common.dto;

/**
 * @author renuka.bhagwat
 *
 */

import java.io.Serializable;
import java.util.Date;

public class TransactionDetailsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// TransactionSummary Specific attributes.Need to be removed
	private Date billCreatedDate;
	private Date billPayDate;
	private String billId;
	private String mfsBillId;
	private Double billAmount;
	private String merchantMsisdn;
	private String merchantCode;
	private String spCode;
	private String rpCode;
	private String status;
	private String billCurrency;

	// TransactionDetails Specific attributes
	private String transSummary;
	private String message;
	private String merchantCountry;
	private String billType;
	private String sendFx;
	private String spSettlementFx;
	private Double sendAmount;
	private Double sendFee;
	private String receiveFx;
	private String rpSettlementFx;
	private Double grossBillAmount;
	private Double taxRate;
	private Double processingFee;
	private Double settlementAmount;
	private Double fxRate;
	private Double margin;
	private String billRefundability;
	private String senderMsisdn;

	/**
	 * @return the billCreatedDate
	 */
	public Date getBillCreatedDate() {
		return billCreatedDate;
	}

	/**
	 * @param billCreatedDate
	 *            the billCreatedDate to set
	 */
	public void setBillCreatedDate(Date billCreatedDate) {
		this.billCreatedDate = billCreatedDate;
	}

	/**
	 * @return the billPayDate
	 */
	public Date getBillPayDate() {
		return billPayDate;
	}

	/**
	 * @param billPayDate
	 *            the billPayDate to set
	 */
	public void setBillPayDate(Date billPayDate) {
		this.billPayDate = billPayDate;
	}

	/**
	 * @return the billId
	 */
	public String getBillId() {
		return billId;
	}

	/**
	 * @param billId
	 *            the billId to set
	 */
	public void setBillId(String billId) {
		this.billId = billId;
	}

	/**
	 * @return the mfsBillId
	 */
	public String getMfsBillId() {
		return mfsBillId;
	}

	/**
	 * @param mfsBillId
	 *            the mfsBillId to set
	 */
	public void setMfsBillId(String mfsBillId) {
		this.mfsBillId = mfsBillId;
	}

	/**
	 * @return the billAmount
	 */
	public Double getBillAmount() {
		return billAmount;
	}

	/**
	 * @param billAmount
	 *            the billAmount to set
	 */
	public void setBillAmount(Double billAmount) {
		this.billAmount = billAmount;
	}

	/**
	 * @return the merchantMsisdn
	 */
	public String getMerchantMsisdn() {
		return merchantMsisdn;
	}

	/**
	 * @param merchantMsisdn
	 *            the merchantMsisdn to set
	 */
	public void setMerchantMsisdn(String merchantMsisdn) {
		this.merchantMsisdn = merchantMsisdn;
	}

	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode() {
		return merchantCode;
	}

	/**
	 * @param merchantCode
	 *            the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	/**
	 * @return the spCode
	 */
	public String getSpCode() {
		return spCode;
	}

	/**
	 * @param spCode
	 *            the spCode to set
	 */
	public void setSpCode(String spCode) {
		this.spCode = spCode;
	}

	/**
	 * @return the rpCode
	 */
	public String getRpCode() {
		return rpCode;
	}

	/**
	 * @param rpCode
	 *            the rpCode to set
	 */
	public void setRpCode(String rpCode) {
		this.rpCode = rpCode;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the billCurrency
	 */
	public String getBillCurrency() {
		return billCurrency;
	}

	/**
	 * @param billCurrency
	 *            the billCurrency to set
	 */
	public void setBillCurrency(String billCurrency) {
		this.billCurrency = billCurrency;
	}

	/**
	 * @return the transSummary
	 */
	public String getTransSummary() {
		return transSummary;
	}

	/**
	 * @param transSummary
	 *            the transSummary to set
	 */
	public void setTransSummary(String transSummary) {
		this.transSummary = transSummary;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the merchantCountry
	 */
	public String getMerchantCountry() {
		return merchantCountry;
	}

	/**
	 * @param merchantCountry
	 *            the merchantCountry to set
	 */
	public void setMerchantCountry(String merchantCountry) {
		this.merchantCountry = merchantCountry;
	}

	/**
	 * @return the billType
	 */
	public String getBillType() {
		return billType;
	}

	/**
	 * @param billType
	 *            the billType to set
	 */
	public void setBillType(String billType) {
		this.billType = billType;
	}

	/**
	 * @return the sendFx
	 */
	public String getSendFx() {
		return sendFx;
	}

	/**
	 * @param sendFx
	 *            the sendFx to set
	 */
	public void setSendFx(String sendFx) {
		this.sendFx = sendFx;
	}

	/**
	 * @return the spSettlementFx
	 */
	public String getSpSettlementFx() {
		return spSettlementFx;
	}

	/**
	 * @param spSettlementFx
	 *            the spSettlementFx to set
	 */
	public void setSpSettlementFx(String spSettlementFx) {
		this.spSettlementFx = spSettlementFx;
	}

	/**
	 * @return the sendAmount
	 */
	public Double getSendAmount() {
		return sendAmount;
	}

	/**
	 * @param sendAmount
	 *            the sendAmount to set
	 */
	public void setSendAmount(Double sendAmount) {
		this.sendAmount = sendAmount;
	}

	/**
	 * @return the sendFee
	 */
	public Double getSendFee() {
		return sendFee;
	}

	/**
	 * @param sendFee
	 *            the sendFee to set
	 */
	public void setSendFee(Double sendFee) {
		this.sendFee = sendFee;
	}

	/**
	 * @return the receiveFx
	 */
	public String getReceiveFx() {
		return receiveFx;
	}

	/**
	 * @param receiveFx
	 *            the receiveFx to set
	 */
	public void setReceiveFx(String receiveFx) {
		this.receiveFx = receiveFx;
	}

	/**
	 * @return the rpSettlementFx
	 */
	public String getRpSettlementFx() {
		return rpSettlementFx;
	}

	/**
	 * @param rpSettlementFx
	 *            the rpSettlementFx to set
	 */
	public void setRpSettlementFx(String rpSettlementFx) {
		this.rpSettlementFx = rpSettlementFx;
	}

	/**
	 * @return the grossBillAmount
	 */
	public Double getGrossBillAmount() {
		return grossBillAmount;
	}

	/**
	 * @param grossBillAmount
	 *            the grossBillAmount to set
	 */
	public void setGrossBillAmount(Double grossBillAmount) {
		this.grossBillAmount = grossBillAmount;
	}

	/**
	 * @return the taxRate
	 */
	public Double getTaxRate() {
		return taxRate;
	}

	/**
	 * @param taxRate
	 *            the taxRate to set
	 */
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	/**
	 * @return the processingFee
	 */
	public Double getProcessingFee() {
		return processingFee;
	}

	/**
	 * @param processingFee
	 *            the processingFee to set
	 */
	public void setProcessingFee(Double processingFee) {
		this.processingFee = processingFee;
	}

	/**
	 * @return the settlementAmount
	 */
	public Double getSettlementAmount() {
		return settlementAmount;
	}

	/**
	 * @param settlementAmount
	 *            the settlementAmount to set
	 */
	public void setSettlementAmount(Double settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	/**
	 * @return the fxRate
	 */
	public Double getFxRate() {
		return fxRate;
	}

	/**
	 * @param fxRate
	 *            the fxRate to set
	 */
	public void setFxRate(Double fxRate) {
		this.fxRate = fxRate;
	}

	/**
	 * @return the margin
	 */
	public Double getMargin() {
		return margin;
	}

	/**
	 * @param margin
	 *            the margin to set
	 */
	public void setMargin(Double margin) {
		this.margin = margin;
	}

	/**
	 * @return the billRefundability
	 */
	public String getBillRefundability() {
		return billRefundability;
	}

	/**
	 * @param billRefundability
	 *            the billRefundability to set
	 */
	public void setBillRefundability(String billRefundability) {
		this.billRefundability = billRefundability;
	}

	/**
	 * @return the senderMsisdn
	 */
	public String getSenderMsisdn() {
		return senderMsisdn;
	}

	/**
	 * @param senderMsisdn
	 *            the senderMsisdn to set
	 */
	public void setSenderMsisdn(String senderMsisdn) {
		this.senderMsisdn = senderMsisdn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionDetailsDto [billCreatedDate=" + billCreatedDate + ", billPayDate=" + billPayDate
				+ ", billId=" + billId + ", mfsBillId=" + mfsBillId + ", billAmount=" + billAmount + ", merchantMsisdn="
				+ merchantMsisdn + ", merchantCode=" + merchantCode + ", spCode=" + spCode + ", rpCode=" + rpCode
				+ ", status=" + status + ", billCurrency=" + billCurrency + ", transSummary=" + transSummary
				+ ", message=" + message + ", merchantCountry=" + merchantCountry + ", billType=" + billType
				+ ", sendFx=" + sendFx + ", spSettlementFx=" + spSettlementFx + ", sendAmount=" + sendAmount
				+ ", sendFee=" + sendFee + ", receiveFx=" + receiveFx + ", rpSettlementFx=" + rpSettlementFx
				+ ", grossBillAmount=" + grossBillAmount + ", taxRate=" + taxRate + ", processingFee=" + processingFee
				+ ", settlementAmount=" + settlementAmount + ", fxRate=" + fxRate + ", margin=" + margin
				+ ", billRefundability=" + billRefundability + ", senderMsisdn=" + senderMsisdn + "]";
	}

}
