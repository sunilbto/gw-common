/**
 * 
 */
package com.mfs.mdm.common.type;

/**
 * @author renuka.bhagwat
 *
 */
public enum ReportType {
	
	ANALYTICS("Analytics"),
	TRANSACTIONS("Transactions"),
	TRANSACTION_DETAILS("Transaction details");
	private String code;
	
	ReportType(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
