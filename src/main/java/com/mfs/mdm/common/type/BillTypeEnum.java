package com.mfs.mdm.common.type;

public enum BillTypeEnum {
	PAPER("PAPER");
	private String code;

	BillTypeEnum(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
