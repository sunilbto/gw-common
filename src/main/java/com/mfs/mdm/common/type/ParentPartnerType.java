package com.mfs.mdm.common.type;
/**
 * @author renuka.bhagwat
 *
 */
public enum ParentPartnerType {

	MNO("MNO"),
	BANK("Bank");
	private String code;
	
	ParentPartnerType(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
