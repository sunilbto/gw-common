package com.mfs.mdm.common.type;

/**
 * @author apurva.patil
 *
 */
public enum IdType {
	NATIONAL_ID("National Id");
	private String code;

	IdType(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

}
