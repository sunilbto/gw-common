package com.mfs.mdm.common.type;

public enum CategoryType {
	SCHOOL("School");
	private String code;

	CategoryType(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
