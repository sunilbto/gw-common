package com.mfs.mdm.common.type;

/**
 * @author apurva.patil
 *
 */
public enum StatusType {
	Active,
	PENDING,
	CLOSED,
	SUSPENDED,
	BLOCKED;
}
