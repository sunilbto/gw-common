package com.mfs.mdm.common.type;

/**
 * @author apurva.patil
 *
 */
public enum RoleType {
	ADMIN("Administrator"), 
	READ_ONLY("ReadOnly"),
	READ_WRITE("ReadWrite");
	private String code;

	RoleType(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
