package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "menu_access")
public class MenuAccess {
	@Id
	@GeneratedValue
	@Column(name = "menu_access_id")
	private int menuAccessId;
	@OneToOne
	@JoinColumn(name = "menu_id")
	private Menu menu;
	
	@Column(name = "menu_access_name")
	private String manuAccessName;

	public int getMenuAccessId() {
		return menuAccessId;
	}

	public void setMenuAccessId(int menuAccessId) {
		this.menuAccessId = menuAccessId;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setManu(Menu menu) {
		this.menu = menu;
	}

	public String getManuAccessName() {
		return manuAccessName;
	}

	public void setManuAccessName(String manuAccessName) {
		this.manuAccessName = manuAccessName;
	}

	@Override
	public String toString() {
		return "ManuAccess [menuAccessId=" + menuAccessId + ", menu=" + menu + ", manuAccessName=" + manuAccessName
				+ "]";
	}

}
