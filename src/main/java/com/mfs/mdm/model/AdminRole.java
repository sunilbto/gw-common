package com.mfs.mdm.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "admin_role")
public class AdminRole {

	@Id
	@GeneratedValue
	@Column(name = "admin_role_id")
	private int adminRoleId;
	@Column(name = "role_name")
	private String roleName;
	@Column(name = "role_description")
	private String roleDescription;
	@Column(name = "creation_date")
	private Date createdOn;
	@Column(name = "created_by")
	private String createdBy;
	@OneToOne
	@JoinColumn(name = "role_type_id")
	private RoleType roleType;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "adminRole")
	private Set<AdminRoleAccess> adminRoleAccesses = new HashSet<AdminRoleAccess>(0);

	public Set<AdminRoleAccess> getAdminRoleAccesses() {
		return adminRoleAccesses;
	}

	public void setAdminRoleAccesses(Set<AdminRoleAccess> adminRoleAccesses) {
		this.adminRoleAccesses = adminRoleAccesses;
	}

	public int getAdminRoleId() {
		return adminRoleId;
	}

	public void setAdminRoleId(int adminRoleId) {
		this.adminRoleId = adminRoleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public RoleType getRoleType() {
		return roleType;
	}

	public void setRoleType(RoleType roleType) {
		this.roleType = roleType;
	}

	@Override
	public String toString() {
		return "AdminRole [adminRoleId=" + adminRoleId + ", roleName=" + roleName + ", roleDescription="
				+ roleDescription + ", createdOn=" + createdOn + ", createdBy=" + createdBy + ", roleType=" + roleType
				+ "]";
	}

}
