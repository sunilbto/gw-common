package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "admin_role_access")
public class AdminRoleAccess {
	@Id
	@GeneratedValue
	@Column(name = "admin_role_access_id")
	private int adminRoleAccessId;
	@OneToOne
	@JoinColumn(name = "admin_role_id")
	private AdminRole adminRole;
	@OneToOne
	@JoinColumn(name = "menu_access_id")
	private MenuAccess menuAccess;

	public int getAdminRoleAccessId() {
		return adminRoleAccessId;
	}

	public void setAdminRoleAccessId(int adminRoleAccessId) {
		this.adminRoleAccessId = adminRoleAccessId;
	}

	public AdminRole getAdminRole() {
		return adminRole;
	}

	public void setAdminRole(AdminRole adminRole) {
		this.adminRole = adminRole;
	}

	public MenuAccess getMenuAccess() {
		return menuAccess;
	}

	public void setManuAccess(MenuAccess menuAccess) {
		this.menuAccess = menuAccess;
	}

	@Override
	public String toString() {
		return "AdminRoleAccess [adminRoleAccessId=" + adminRoleAccessId + ", adminRole=" + adminRole
				+ ", menuAccess=" + menuAccess + "]";
	}

}
