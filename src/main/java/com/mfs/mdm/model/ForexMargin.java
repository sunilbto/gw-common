package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "world_bank_margin")
public class ForexMargin {
	
	@Id
	@GeneratedValue
	@Column(name = "world_bank_margin_id")
	private int worldBankMarginId;
	@Column(name = "forex_margin")
	private double forexMargin;
	@Column(name = "wb_status")
	private String wbStatus;
	public int getWorldBankMarginId() {
		return worldBankMarginId;
	}
	public void setWorldBankMarginId(int worldBankMarginId) {
		this.worldBankMarginId = worldBankMarginId;
	}
	public double getForexMargin() {
		return forexMargin;
	}
	public void setForexMargin(double forexMargin) {
		this.forexMargin = forexMargin;
	}
	public String getWbStatus() {
		return wbStatus;
	}
	public void setWbStatus(String wbStatus) {
		this.wbStatus = wbStatus;
	}
	@Override
	public String toString() {
		return "ForexMargin [worldBankMarginId=" + worldBankMarginId + ", forexMargin=" + forexMargin + ", wbStatus="
				+ wbStatus + "]";
	}
	

}
