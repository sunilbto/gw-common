package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "state_region")
public class StateRegion {

	@Id
	@GeneratedValue
	@Column(name = "state_region_id")
	private int stateRegionId;
	@Column(name = "state_region_name")
	private String stateRegionName;
	@OneToOne
	@JoinColumn(name = "country_id")
	private CountryMaster countryMaster;

	public int getStateRegionId() {
		return stateRegionId;
	}

	public void setStateRegionId(int stateRegionId) {
		this.stateRegionId = stateRegionId;
	}

	public String getStateRegionName() {
		return stateRegionName;
	}

	public void setStateRegionName(String stateRegionName) {
		this.stateRegionName = stateRegionName;
	}

	public CountryMaster getCountryMaster() {
		return countryMaster;
	}

	public void setCountryMaster(CountryMaster countryMaster) {
		this.countryMaster = countryMaster;
	}

	@Override
	public String toString() {
		return "StateRegion [stateRegionId=" + stateRegionId + ", stateRegionName=" + stateRegionName
				+ ", countryMaster=" + countryMaster + "]";
	}

}
