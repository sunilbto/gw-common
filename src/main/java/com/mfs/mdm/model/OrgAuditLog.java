package com.mfs.mdm.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "org_audit_log")
public class OrgAuditLog {
	@Id
	@GeneratedValue
	@Column(name = "org_audit_log_id")
	private long orgAuditLogId;
	@Column(name = "org_name")
	private String orgName;
	@Column(name = "user_name")
	private String userName;
	@Column(name = "action_name")
	private String actionName;
	@Column(name = "action_item")
	private String actionItem;
	@Column(name = "action_item_value")
	private String actionItemValue;
	@Column(name = "creation_date")
	private Date ceationDate;

	public long getOrgAuditLogId() {
		return orgAuditLogId;
	}

	public void setOrgAuditLogId(long orgAuditLogId) {
		this.orgAuditLogId = orgAuditLogId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getActionItem() {
		return actionItem;
	}

	public void setActionItem(String actionItem) {
		this.actionItem = actionItem;
	}

	public String getActionItemValue() {
		return actionItemValue;
	}

	public void setActionItemValue(String actionItemValue) {
		this.actionItemValue = actionItemValue;
	}

	public Date getCeationDate() {
		return ceationDate;
	}

	public void setCeationDate(Date ceationDate) {
		this.ceationDate = ceationDate;
	}

	@Override
	public String toString() {
		return "OrgAuditLog [orgAuditLogId=" + orgAuditLogId + ", orgName=" + orgName + ", userName=" + userName
				+ ", actionName=" + actionName + ", actionItem=" + actionItem + ", actionItemValue=" + actionItemValue
				+ ", ceationDate=" + ceationDate + "]";
	}

}
