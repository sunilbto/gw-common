package com.mfs.mdm.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "organisation_role")
public class OrganizationRole {
	@Id
	@GeneratedValue
	@Column(name = "organisation_role_id")
	private int organisationRoleId;
	@Column(name = "role_name")
	private String roleName;
	@Column(name = "role_description")
	private String roleDescription;
	@Column(name = "creation_date")
	private Date creationDate;
	@Column(name = "created_by")
	private String createdBy;
	@OneToOne
	@JoinColumn(name = "role_type_id")
	private RoleType roleType;

	@Column(name = "organisation_id")
	private int organizationId;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "organizationRole")
	private Set<OrgRoleAccess> orgRoleAccesses = new HashSet<OrgRoleAccess>(0);

	@OneToOne
	@JoinColumn(name = "org_type_id")
	private OrganizationType organizationType;

	public Set<OrgRoleAccess> getOrgRoleAccesses() {
		return orgRoleAccesses;
	}

	public void setOrgRoleAccesses(Set<OrgRoleAccess> orgRoleAccesses) {
		this.orgRoleAccesses = orgRoleAccesses;
	}

	public int getOrganisationRoleId() {
		return organisationRoleId;
	}

	public void setOrganisationRoleId(int organisationRoleId) {
		this.organisationRoleId = organisationRoleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public RoleType getRoleType() {
		return roleType;
	}

	public void setRoleType(RoleType roleType) {
		this.roleType = roleType;
	}

	public int getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}

	public OrganizationType getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(OrganizationType organizationType) {
		this.organizationType = organizationType;
	}

	@Override
	public String toString() {
		return "OrganizationRole [organisationRoleId=" + organisationRoleId + ", roleName=" + roleName
				+ ", roleDescription=" + roleDescription + ", creationDate=" + creationDate + ", createdBy=" + createdBy
				+ ", roleType=" + roleType + ", organizationId=" + organizationId + ", orgRoleAccesses="
				+ orgRoleAccesses + ", organizationType=" + organizationType + "]";
	}

}
