package com.mfs.mdm.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "organisation_user")
public class OrganizationUser {
	@Id
	@GeneratedValue
	@Column(name = "organisation_user_id")
	private int organisationUserId;
	@Column(name = "password")
	private String password;
	@Column(name = "is_account_locked")
	private boolean isAccountLocked;
	@Column(name = "reset_password_created_on")
	private Date resetPasswordCreatedOn;
	@Column(name = "reset_password")
	private String resetPassword;
	@Column(name = "is_password_status")
	private boolean isPasswordStatus;
	@OneToOne
	@JoinColumn(name = "organisation_id")
	private Organization organization;
	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "organizationUser")
	private Set<OrganizationUserRole> organizationUserRoles = new HashSet<OrganizationUserRole>(0);

	public int getOrganisationUserId() {
		return organisationUserId;
	}

	public void setOrganisationUserId(int organisationUserId) {
		this.organisationUserId = organisationUserId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getIsAccountLocked() {
		return isAccountLocked;
	}

	public void setIsAccountLocked(boolean isAccountLocked) {
		this.isAccountLocked = isAccountLocked;
	}

	public Date getResetPasswordCreatedOn() {
		return resetPasswordCreatedOn;
	}

	public void setResetPasswordCreatedOn(Date resetPasswordCreatedOn) {
		this.resetPasswordCreatedOn = resetPasswordCreatedOn;
	}

	public String getResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}

	public boolean getIsPasswordStatus() {
		return isPasswordStatus;
	}

	public void setIsPasswordStatus(boolean isPasswordStatus) {
		this.isPasswordStatus = isPasswordStatus;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<OrganizationUserRole> getOrganizationUserRoles() {
		return this.organizationUserRoles;
	}

	public void setOrganizationUserRoles(Set<OrganizationUserRole> organizationUserRoles) {
		this.organizationUserRoles = organizationUserRoles;
	}

	@Override
	public String toString() {
		return "OrganizationUser [organisationUserId=" + organisationUserId + ", password=" + password
				+ ", isAccountLocked=" + isAccountLocked + ", resetPasswordCreatedOn=" + resetPasswordCreatedOn
				+ ", resetPassword=" + resetPassword + ", isPasswordStatus=" + isPasswordStatus + ", organization="
				+ organization + ", user=" + user + ", organizationUserRoles=" + organizationUserRoles + "]";
	}

}
