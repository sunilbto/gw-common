package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role_type")
public class RoleType {
	@Id
	@GeneratedValue
	@Column(name = "role_type_id")
	private int roleTypeId;
	@Column(name = "role_type_name")
	private String roleTypeName;
	@Column(name = "description")
	private String description;

	public int getRoleTypeId() {
		return roleTypeId;
	}

	public void setRoleTypeId(int roleTypeId) {
		this.roleTypeId = roleTypeId;
	}

	public String getRoleTypeName() {
		return roleTypeName;
	}

	public void setRoleTypeName(String roleTypeName) {
		this.roleTypeName = roleTypeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "RoleType [roleTypeId=" + roleTypeId + ", roleTypeName=" + roleTypeName + ", description=" + description
				+ "]";
	}

}
