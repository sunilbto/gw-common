package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "organisation_user_role")
public class OrganizationUserRole {
	@Id
	@GeneratedValue
	@Column(name = "organisation_user_role_id")
	private int organisationUserRoleId;
	@Column(name = "is_role_active")
	private boolean isRoleActive;
	@OneToOne
	@JoinColumn(name = "organisation_user_id")
	private OrganizationUser organizationUser;
	@OneToOne
	@JoinColumn(name = "organisation_role_id")
	private OrganizationRole organizationRole;

	public int getOrganisationUserRoleId() {
		return organisationUserRoleId;
	}

	public void setOrganisationUserRoleId(int organisationUserRoleId) {
		this.organisationUserRoleId = organisationUserRoleId;
	}

	public boolean getIsRoleActive() {
		return isRoleActive;
	}

	public void setIsRoleActive(boolean isRoleActive) {
		this.isRoleActive = isRoleActive;
	}

	public OrganizationUser getOrganizationUser() {
		return organizationUser;
	}

	public void setOrganizationUser(OrganizationUser organizationUser) {
		this.organizationUser = organizationUser;
	}

	public OrganizationRole getOrganizationRole() {
		return organizationRole;
	}

	public void setOrganizationRole(OrganizationRole organizationRole) {
		this.organizationRole = organizationRole;
	}

	@Override
	public String toString() {
		return "OrganizationUserRole [organisationUserRoleId=" + organisationUserRoleId + ", isRoleActive="
				+ isRoleActive + ", organizationUser=" + organizationUser + ", organizationRole=" + organizationRole
				+ "]";
	}

}
