package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "partner_type")
public class PartnerType {

	@Id
	@GeneratedValue
	@Column(name = "partner_type_id")
	private int partnerTypeId;

	@Column(name = "partner_type")
	private String partnerType;

	public int getPartnerTypeId() {
		return partnerTypeId;
	}

	public void setPartnerTypeId(int partnerTypeId) {
		this.partnerTypeId = partnerTypeId;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	@Override
	public String toString() {
		return "PartnerType [partnerTypeId=" + partnerTypeId + ", partnerType=" + partnerType + "]";
	}

}
