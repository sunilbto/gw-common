package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "org_group")
public class OrgGroup {
	@Id
	@GeneratedValue
	@Column(name = "org_group_id")
	private int orgGroupId;
	@OneToOne
	@JoinColumn(name = "group_id")
	private Group group;
	@Column(name = "organisation_id")
	private int orgId;

	public int getOrgGroupId() {
		return orgGroupId;
	}

	public void setOrgGroupId(int orgGroupId) {
		this.orgGroupId = orgGroupId;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	

	public int getOrgId() {
		return orgId;
	}

	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	@Override
	public String toString() {
		return "orgGroup [orgGroupId=" + orgGroupId + ", group=" + group + ", orgId=" + orgId + "]";
	}

}
