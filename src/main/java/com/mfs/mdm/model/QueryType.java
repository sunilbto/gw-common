package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "type_of_query")
public class QueryType {

	@Id
	@GeneratedValue
	@Column(name = "type_Of_query_id")
	private int typeOfQueryId;
	@Column(name = "query_type")
	private String queryType;
	public int getTypeOfQueryId() {
		return typeOfQueryId;
	}
	public void setTypeOfQueryId(int typeOfQueryId) {
		this.typeOfQueryId = typeOfQueryId;
	}
	public String getQueryType() {
		return queryType;
	}
	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
	@Override
	public String toString() {
		return "QueryType [typeOfQueryId=" + typeOfQueryId + ", queryType=" + queryType + "]";
	}
	
	
	
}
