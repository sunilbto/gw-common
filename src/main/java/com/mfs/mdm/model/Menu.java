package com.mfs.mdm.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "menu")
public class Menu {
	@Id
	@GeneratedValue
	@Column(name = "menu_id")
	private int manuId;
	@Column(name = "menu_name")
	private String manuName;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "menu")
	private Set<MenuAccess> menuAccesses = new HashSet<MenuAccess>(0);
	
	public Set<MenuAccess> getMenuAccesses() {
		return menuAccesses;
	}
	public void setMenuAccesses(Set<MenuAccess> menuAccesses) {
		this.menuAccesses = menuAccesses;
	}
	public int getManuId() {
		return manuId;
	}
	public void setManuId(int manuId) {
		this.manuId = manuId;
	}
	public String getManuName() {
		return manuName;
	}
	public void setManuName(String manuName) {
		this.manuName = manuName;
	}
	@Override
	public String toString() {
		return "Manu [manuId=" + manuId + ", manuName=" + manuName + "]";
	}
	
	
}
