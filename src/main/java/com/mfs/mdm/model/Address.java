package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "address")
public class Address {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int addressId;
	@Column(name = "address")
	private String address;
	@Column(name = "address1")
	private String address1;
	@Column(name = "address2")
	private String address2;
	@Column(name = "postal_code")
	private String postalCode;
	@Column(name = "city")
	private String city;
	@Column(name = "state_region")
	private String stateRegion;
	@OneToOne
	@JoinColumn(name = "country_id")
	private CountryMaster countryMaster;
	//@OneToOne
	//@JoinColumn(name = "organisation_id")
	@Column(name="organisation_id")
	private int organization;
	//@OneToOne
	//@JoinColumn(name="user_id")
	@Column(name="user_id")
	private int user;
	
	
	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateRegion() {
		return stateRegion;
	}

	public void setStateRegion(String stateRegion) {
		this.stateRegion = stateRegion;
	}

	public CountryMaster getCountryMaster() {
		return countryMaster;
	}

	public void setCountryMaster(CountryMaster countryMaster) {
		this.countryMaster = countryMaster;
	}

	public int getOrganization() {
		return organization;
	}

	public void setOrganization(int organization) {
		this.organization = organization;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Address [addressId=" + addressId + ", address=" + address + ", address1=" + address1 + ", address2="
				+ address2 + ", postalCode=" + postalCode + ", city=" + city + ", stateRegion=" + stateRegion
				+ ", countryMaster=" + countryMaster + ", organization=" + organization + ", user=" + user + "]";
	}


	

	

}
