package com.mfs.mdm.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "admin_user")
public class AdminUser {
	@Id
	@GeneratedValue
	@Column(name = "admin_user_id")
	private int adminUserId;
	@Column(name = "password")
	private String password;
	@Column(name = "reset_password")
	private String resetPassword;
	@Column(name = "reset_password_created_on")
	private Date resetPasswordCreatedOn;
	@Column(name = "is_password_status")
	private boolean isPasswordStatus;
	@Column(name = "is_account_locked")
	private boolean isAccountLocked;
	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "adminUser")
	private Set<AdminUserRole> adminUserRoles = new HashSet<AdminUserRole>(0);

	public int getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(int adminUserId) {
		this.adminUserId = adminUserId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}

	public Date getResetPasswordCreatedOn() {
		return resetPasswordCreatedOn;
	}

	public void setResetPasswordCreatedOn(Date resetPasswordCreatedOn) {
		this.resetPasswordCreatedOn = resetPasswordCreatedOn;
	}

	public boolean getIsPasswordStatus() {
		return isPasswordStatus;
	}

	public void setIsPasswordStatus(boolean isPasswordStatus) {
		this.isPasswordStatus = isPasswordStatus;
	}

	public boolean getIsAccountLocked() {
		return isAccountLocked;
	}

	public void setIsAccountLocked(boolean isAccountLocked) {
		this.isAccountLocked = isAccountLocked;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<AdminUserRole> getAdminUserRoles() {
		return this.adminUserRoles;
	}

	public void setAdminUserRoles(Set<AdminUserRole> adminUserRoles) {
		this.adminUserRoles = adminUserRoles;
	}

	@Override
	public String toString() {
		return "AdminUser [adminUserId=" + adminUserId + ", password=" + password + ", resetPassword=" + resetPassword
				+ ", resetPasswordCreatedOn=" + resetPasswordCreatedOn + ", isPasswordStatus=" + isPasswordStatus
				+ ", isAccountLocked=" + isAccountLocked + ", user=" + user + ", adminUserRoles=" + adminUserRoles
				+ "]";
	}

}
