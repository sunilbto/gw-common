package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "country_zone")
public class CountryZone {
	@Id
	@GeneratedValue
	@Column(name = "country_zone_id")
	private int countryZoneId;
	@OneToOne
	@JoinColumn(name = "zone_id")
	private Zone zone;
	@OneToOne
	@JoinColumn(name = "country_id")
	private CountryMaster countryMaster;

	public int getCountryZoneId() {
		return countryZoneId;
	}

	public void setCountryZoneId(int countryZoneId) {
		this.countryZoneId = countryZoneId;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public CountryMaster getCountryMaster() {
		return countryMaster;
	}

	public void setCountryMaster(CountryMaster countryMaster) {
		this.countryMaster = countryMaster;
	}

	@Override
	public String toString() {
		return "CountryZone [countryZoneId=" + countryZoneId + ", zone=" + zone + ", countryMaster=" + countryMaster
				+ "]";
	}

}
