package com.mfs.mdm.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue
	@Column(name = "user_id")
	private int userId;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "id_expiry_date")
	private String idExpiryDate;
	@Column(name = "email")
	private String email;
	@Column(name = "is_user_active")
	private boolean isUserActive;
	@Column(name = "status")
	private String status;
	@Column(name = "id_number")
	private String idNumber;
	@Column(name = "date_of_birth")
	private Date dob;
	@Column(name = "is_email_verified")
	private boolean isEmailVerified;
	@Column(name = "first_activation_date")
	private Date firstActivationDate;
	@Column(name = "mobile_number")
	private String mobile;
	@OneToOne
	@JoinColumn(name = "time_zone_id")
	private TimeZone timeZone;
	@OneToOne
	@JoinColumn(name = "group_id")
	private Group group;
	
	@OneToOne
	@JoinColumn(name = "country_id")
	private CountryMaster countryId;

	@OneToOne
	@JoinColumn(name = "id_type_id")
	private IdType idType;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<AdminUser> adminUsers = new HashSet<AdminUser>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<OrganizationUser> organizationUsers = new HashSet<OrganizationUser>(0);

	public Set<AdminUser> getAdminUsers() {
		return this.adminUsers;
	}

	public void setAdminUsers(Set<AdminUser> adminUsers) {
		this.adminUsers = adminUsers;
	}

	public Set<OrganizationUser> getOrganizationUsers() {
		return this.organizationUsers;
	}

	public void setOrganizationUsers(Set<OrganizationUser> organizationUsers) {
		this.organizationUsers = organizationUsers;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIdExpiryDate() {
		return idExpiryDate;
	}

	public void setIdExpiryDate(String idExpiryDate) {
		this.idExpiryDate = idExpiryDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean getIsUserActive() {
		return isUserActive;
	}

	public void setIsUserActive(boolean isUserActive) {
		this.isUserActive = isUserActive;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public boolean getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public Date getFirstActivationDate() {
		return firstActivationDate;
	}

	public void setFirstActivationDate(Date firstActivationDate) {
		this.firstActivationDate = firstActivationDate;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setUserActive(boolean isUserActive) {
		this.isUserActive = isUserActive;
	}

	public void setEmailVerified(boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public IdType getIdType() {
		return idType;
	}

	public void setIdType(IdType idType) {
		this.idType = idType;
	}

	public CountryMaster getCountryId() {
		return countryId;
	}

	public void setCountryId(CountryMaster countryId) {
		this.countryId = countryId;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", idExpiryDate="
				+ idExpiryDate + ", email=" + email + ", isUserActive=" + isUserActive + ", status=" + status
				+ ", idNumber=" + idNumber + ", dob=" + dob + ", isEmailVerified=" + isEmailVerified
				+ ", firstActivationDate=" + firstActivationDate + ", mobile=" + mobile + ", timeZone=" + timeZone
				+ ", group=" + group + ", countryId=" + countryId + ", idType=" + idType + ", adminUsers=" + adminUsers
				+ ", organizationUsers=" + organizationUsers + "]";
	}

	

}
