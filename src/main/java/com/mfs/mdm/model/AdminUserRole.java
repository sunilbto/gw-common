package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "admin_user_role")
public class AdminUserRole {

	@Id
	@GeneratedValue
	@Column(name = "admin_user_role_id")
	private int adminUserRoleId;
	@Column(name = "is_role_active")
	private boolean isRoleActive;
	@OneToOne
	@JoinColumn(name = "admin_role_id")
	private AdminRole adminRole;
	@OneToOne
	@JoinColumn(name = "admin_user_id")
	private AdminUser adminUser;

	public int getAdminUserRoleId() {
		return adminUserRoleId;
	}

	public void setAdminUserRoleId(int adminUserRoleId) {
		this.adminUserRoleId = adminUserRoleId;
	}

	public boolean getIsRoleActive() {
		return isRoleActive;
	}

	public void setIsRoleActive(boolean isRoleActive) {
		this.isRoleActive = isRoleActive;
	}

	public AdminRole getAdminRole() {
		return adminRole;
	}

	public void setAdminRole(AdminRole adminRole) {
		this.adminRole = adminRole;
	}

	public AdminUser getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(AdminUser adminUser) {
		this.adminUser = adminUser;
	}

	@Override
	public String toString() {
		return "AdminUserRole [adminUserRoleId=" + adminUserRoleId + ", isRoleActive=" + isRoleActive + ", adminRole="
				+ adminRole + ", adminUser=" + adminUser + "]";
	}

}
