package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "contact_details")
public class ContactDetails {
	@Id
	@GeneratedValue
	@Column(name = "contact_details_id")
	private int contactDetailsId;
	@Column(name = "email")
	private String email;
	@Column(name = "partner_name")
	private String partnerName;
	@Column(name = "comment")
	private String comment;
	@OneToOne
	@JoinColumn(name="type_Of_query_id")
	private QueryType queryType;
	public int getContactDetailsId() {
		return contactDetailsId;
	}
	public void setContactDetailsId(int contactDetailsId) {
		this.contactDetailsId = contactDetailsId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public QueryType getQueryType() {
		return queryType;
	}
	public void setQueryType(QueryType queryType) {
		this.queryType = queryType;
	}
	@Override
	public String toString() {
		return "ContactDetails [contactDetailsId=" + contactDetailsId + ", email=" + email + ", partnerName="
				+ partnerName + ", comment=" + comment + ", queryType=" + queryType + "]";
	}
	

}
