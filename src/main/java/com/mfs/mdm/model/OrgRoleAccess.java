package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "org_role_access")
public class OrgRoleAccess {
	@Id
	@GeneratedValue
	@Column(name = "org_role_access_id")
	private int orgRoleAccessId;
	
	@OneToOne
	@JoinColumn(name = "organisation_role_id")
	private OrganizationRole organizationRole;
	
	@OneToOne
	@JoinColumn(name = "menu_access_id")
	private MenuAccess menuAccess;

	public int getOrgRoleAccessId() {
		return orgRoleAccessId;
	}

	public void setOrgRoleAccessId(int orgRoleAccessId) {
		this.orgRoleAccessId = orgRoleAccessId;
	}

	public OrganizationRole getOrganizationRole() {
		return organizationRole;
	}

	public void setOrganizationRole(OrganizationRole organizationRole) {
		this.organizationRole = organizationRole;
	}

	public MenuAccess getMenuAccess() {
		return menuAccess;
	}

	public void setManuAccess(MenuAccess menuAccess) {
		this.menuAccess = menuAccess;
	}

	@Override
	public String toString() {
		return "OrgRoleAccess [orgRoleAccessId=" + orgRoleAccessId + ", organizationRole=" + organizationRole
				+ ", menuAccess=" + menuAccess + "]";
	}

}
