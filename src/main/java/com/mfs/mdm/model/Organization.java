package com.mfs.mdm.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "organisation")
public class Organization {
	@Id
	@GeneratedValue
	@Column(name = "organisation_id")
	private int organisationId;
	@Column(name = "org_id")
	private String orgId;
	@Column(name = "org_name")
	private String orgName;
	@Column(name = "business_registration_no")
	private String businessRegNo;
	/*
	 * @Column(name = "category") private String category;
	 */
	@Column(name = "mobile_no")
	private String mobileNumber;
	@Column(name = "email")
	private String email;
	@Column(name = "registration_date")
	private Date regDate;
	@Column(name = "status")
	private String status;
	@Column(name = "first_activation_date")
	private Date firstActivationDate;
	@Column(name = "org_api_key")
	private String orgApiKey;

	@Column(name = "compliance_officer")
	private String complianceOfficer;

	@Column(name = "compliance_email")
	private String complianceEmail;

	@Column(name = "compliance_phone")
	private String compliancePhone;

	@Column(name = "description")
	private String description;

	@OneToOne
	@JoinColumn(name = "regulator_country_id")
	private CountryMaster regulatorCountryId;

	@OneToOne
	@JoinColumn(name = "category_id")
	private Category categories;

	@OneToOne
	@JoinColumn(name = "org_type_id")
	private OrganizationType organizationType;

	@OneToOne
	@JoinColumn(name = "zone_id")
	private Zone zone;

	@OneToOne
	@JoinColumn(name = "currency_id")
	private CurrencyMaster currencyMaster;

	@Column(name = "finance_email")
	private String financeEmail;

	@Column(name = "support_email")
	private String supportEmail;

	@Column(name = "operations_email")
	private String operationsEmail;

	@OneToOne
	@JoinColumn(name = "partner_type_id")
	private PartnerType partnerType;

	public String getFinanceEmail() {
		return financeEmail;
	}

	public void setFinanceEmail(String financeEmail) {
		this.financeEmail = financeEmail;
	}

	public String getSupportEmail() {
		return supportEmail;
	}

	public void setSupportEmail(String supportEmail) {
		this.supportEmail = supportEmail;
	}

	public String getOperationsEmail() {
		return operationsEmail;
	}

	public void setOperationsEmail(String operationsEmail) {
		this.operationsEmail = operationsEmail;
	}

	public CurrencyMaster getCurrencyMaster() {
		return currencyMaster;
	}

	public void setCurrencyMaster(CurrencyMaster currencyMaster) {
		this.currencyMaster = currencyMaster;
	}

	public int getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(int organisationId) {
		this.organisationId = organisationId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getBusinessRegNo() {
		return businessRegNo;
	}

	public void setBusinessRegNo(String businessRegNo) {
		this.businessRegNo = businessRegNo;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getFirstActivationDate() {
		return firstActivationDate;
	}

	public void setFirstActivationDate(Date firstActivationDate) {
		this.firstActivationDate = firstActivationDate;
	}

	public String getOrgApiKey() {
		return orgApiKey;
	}

	public void setOrgApiKey(String orgApiKey) {
		this.orgApiKey = orgApiKey;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Category getCategories() {
		return categories;
	}

	public void setCategories(Category categories) {
		this.categories = categories;
	}

	public OrganizationType getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(OrganizationType organizationType) {
		this.organizationType = organizationType;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public CountryMaster getRegulatorCountryId() {
		return regulatorCountryId;
	}

	public void setRegulatorCountryId(CountryMaster regulatorCountryId) {
		this.regulatorCountryId = regulatorCountryId;
	}

	public String getComplianceOfficer() {
		return complianceOfficer;
	}

	public void setComplianceOfficer(String complianceOfficer) {
		this.complianceOfficer = complianceOfficer;
	}

	public String getComplianceEmail() {
		return complianceEmail;
	}

	public void setComplianceEmail(String complianceEmail) {
		this.complianceEmail = complianceEmail;
	}

	public String getCompliancePhone() {
		return compliancePhone;
	}

	public void setCompliancePhone(String compliancePhone) {
		this.compliancePhone = compliancePhone;
	}

	public PartnerType getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(PartnerType partnerType) {
		this.partnerType = partnerType;
	}

	@Override
	public String toString() {
		return "Organization [organisationId=" + organisationId + ", orgId=" + orgId + ", orgName=" + orgName
				+ ", businessRegNo=" + businessRegNo + ", mobileNumber=" + mobileNumber + ", email=" + email
				+ ", regDate=" + regDate + ", status=" + status + ", firstActivationDate=" + firstActivationDate
				+ ", orgApiKey=" + orgApiKey + ", complianceOfficer=" + complianceOfficer + ", complianceEmail="
				+ complianceEmail + ", compliancePhone=" + compliancePhone + ", description=" + description
				+ ", regulatorCountryId=" + regulatorCountryId + ", categories=" + categories + ", organizationType="
				+ organizationType + ", zone=" + zone + ", currencyMaster=" + currencyMaster + ", financeEmail="
				+ financeEmail + ", supportEmail=" + supportEmail + ", operationsEmail=" + operationsEmail
				+ ", partnerType=" + partnerType + "]";
	}

}
