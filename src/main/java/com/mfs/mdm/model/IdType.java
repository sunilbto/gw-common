package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "id_type")
public class IdType {
	@Id
	@GeneratedValue
	@Column(name = "id_type_id")
	private int idTypeId;
	@Column(name = "type")
	private String idType;

	public int getIdTypeId() {
		return idTypeId;
	}

	public void setIdTypeId(int idTypeId) {
		this.idTypeId = idTypeId;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	@Override
	public String toString() {
		return "IdType [idTypeId=" + idTypeId + ", idType=" + idType + "]";
	}

}
