package com.mfs.mdm.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "zone")
public class Zone {
	@Id
	@GeneratedValue
	@Column(name = "zone_id")
	private int zoneId;
	@Column(name = "zone_name")
	private String zoneName;
	@Column(name = "description")
	private String description;
	@Column(name = "create_date")
	private Date createaDate;
	@Column(name = "created_by")
	private String createdBy;

	public int getZoneId() {
		return zoneId;
	}

	public void setZoneId(int zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateaDate() {
		return createaDate;
	}

	public void setCreateaDate(Date createaDate) {
		this.createaDate = createaDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return "Zone [zoneId=" + zoneId + ", zoneName=" + zoneName + ", description=" + description + ", createaDate="
				+ createaDate + ", createdBy=" + createdBy + "]";
	}

}
