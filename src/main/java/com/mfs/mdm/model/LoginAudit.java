package com.mfs.mdm.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "login_audit")
public class LoginAudit {
	@Id
	@GeneratedValue
	@Column(name = "login_audit_id")
	private long loginAuditId;
	@Column(name = "admin_username")
	private String adminUserName;
	@Column(name = "ip_address")
	private String ipAddress;
	@Column(name = "date_time")
	private Date dateTime;
	@Column(name = "event_result")
	private String eventResult;
	@Column(name = "description")
	private String description;
	@Column(name = "org_name")
	private String orgName;
	@Column(name = "org_username")
	private String orgUserName;
	@Column(name = "status")
	private String status;

	public long getLoginAuditId() {
		return loginAuditId;
	}

	public void setLoginAuditId(long loginAuditId) {
		this.loginAuditId = loginAuditId;
	}

	public String getAdminUserName() {
		return adminUserName;
	}

	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getEventResult() {
		return eventResult;
	}

	public void setEventResult(String eventResult) {
		this.eventResult = eventResult;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgUserName() {
		return orgUserName;
	}

	public void setOrgUserName(String orgUserName) {
		this.orgUserName = orgUserName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "LoginAudit [loginAuditId=" + loginAuditId + ", adminUserName=" + adminUserName + ", ipAddress="
				+ ipAddress + ", dateTime=" + dateTime + ", eventResult=" + eventResult + ", description=" + description
				+ ", orgName=" + orgName + ", orgUserName=" + orgUserName + ", status=" + status + "]";
	}

}
