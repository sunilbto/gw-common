package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="system_config")
public class SystemConfig {
	
@Id
@GeneratedValue
@Column(name="idsystem_config")
private int SystemConfigId;

@Column(name="config_key")
private String configKey;

@Column(name="config_value")
private String configValue;

public int getSystemConfigId() {
	return SystemConfigId;
}

public void setSystemConfigId(int systemConfigId) {
	SystemConfigId = systemConfigId;
}

public String getConfigKey() {
	return configKey;
}

public void setConfigKey(String configKey) {
	this.configKey = configKey;
}

public String getConfigValue() {
	return configValue;
}

public void setConfigValue(String configValue) {
	this.configValue = configValue;
}

@Override
public String toString() {
	return "SystemConfig [SystemConfigId=" + SystemConfigId + ", configKey=" + configKey + ", configValue="
			+ configValue + "]";
}



}
