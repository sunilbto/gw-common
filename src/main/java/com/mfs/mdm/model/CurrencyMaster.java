package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "currency_master")
public class CurrencyMaster {

	@Id
	@GeneratedValue
	@Column(name = "currency_master_id")
	private int currencyId;
	@Column(name = "currency_name")
	private String currencyName;
	@Column(name = "currency_code")
	private String currencyCode;

	public int getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	@Override
	public String toString() {
		return "CurrencyMaster [currencyId=" + currencyId + ", currencyName=" + currencyName + ", currencyCode="
				+ currencyCode + "]";
	}

}
