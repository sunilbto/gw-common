package com.mfs.mdm.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "admin_audit_log")
public class AdminAuditLog {

	@Id
	@GeneratedValue
	@Column(name = "admin_audit_log_id")
	private long adminAuditId;
	@Column(name = "user_name")
	private String userName;
	@Column(name = "action_name")
	private String actonName;
	@Column(name = "action_item")
	private String actionItem;
	@Column(name = "action_item_value")
	private String actionItemValue;
	@Column(name = "creation_date")
	private Date creationDate;

	public long getAdminAuditId() {
		return adminAuditId;
	}

	public void setAdminAuditId(long adminAuditId) {
		this.adminAuditId = adminAuditId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getActonName() {
		return actonName;
	}

	public void setActonName(String actonName) {
		this.actonName = actonName;
	}

	public String getActionItem() {
		return actionItem;
	}

	public void setActionItem(String actionItem) {
		this.actionItem = actionItem;
	}

	public String getActionItemValue() {
		return actionItemValue;
	}

	public void setActionItemValue(String actionItemValue) {
		this.actionItemValue = actionItemValue;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		return "AdminAuditLog [adminAuditId=" + adminAuditId + ", userName=" + userName + ", actonName=" + actonName
				+ ", actionItem=" + actionItem + ", actionItemValue=" + actionItemValue + ", creationDate="
				+ creationDate + "]";
	}

}
