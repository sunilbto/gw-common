package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="organization_type")
public class OrganizationType {
	
	@Id
	@GeneratedValue
	@Column(name="org_type_id")
	private int orgTypeId;
	
	@Column(name="org_type")
	private String orgType;

	public int getOrgTypeId() {
		return orgTypeId;
	}

	public void setOrgTypeId(int orgTypeId) {
		this.orgTypeId = orgTypeId;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	@Override
	public String toString() {
		return "OrganizationType [orgTypeId=" + orgTypeId + ", orgType=" + orgType + "]";
	}
	
	

}
