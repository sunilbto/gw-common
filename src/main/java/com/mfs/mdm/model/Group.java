package com.mfs.mdm.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "group_master")
public class Group {

	@Id
	@GeneratedValue
	@Column(name = "group_id")
	private int groupId;
	@Column(name = "group_name")
	private String groupName;
	@Column(name = "group_description")
	private String description;
	@Column(name = "creation_date")
	private Date ceationDate;

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Date getCeationDate() {
		return ceationDate;
	}

	public void setCeationDate(Date ceationDate) {
		this.ceationDate = ceationDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;

	}

	@Override
	public String toString() {
		return "Group [groupId=" + groupId + ", groupName=" + groupName + ", description=" + description
				+ ", ceationDate=" + ceationDate + "]";
	}

}
