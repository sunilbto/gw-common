package com.mfs.mdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "time_zone_master")
public class TimeZone {
	@Id
	@GeneratedValue
	@Column(name = "time_zone_id")
	private int timeZoneId;
	@Column(name = "time_zone_name")
	private String timeZoneName;
	@Column(name = "time_zone")
	private String timeZone;

	public int getTimeZoneId() {
		return timeZoneId;
	}

	public void setTimeZoneId(int timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	public String getTimeZoneName() {
		return timeZoneName;
	}

	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	@Override
	public String toString() {
		return "TimeZone [timeZoneId=" + timeZoneId + ", timeZoneName=" + timeZoneName + ", timeZone=" + timeZone + "]";
	}

}
